from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Profile(models.Model):
    MAN = 'M'
    WOMEN = 'W'
    OTHER = 'O'
    SEX = (
        (MAN, 'MAN'),
        (WOMEN, 'WOMEN'),
        (OTHER, 'OTHER'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='User')
    cell_phone = models.CharField(max_length=11, verbose_name='Phone no', blank=True)
    location = models.CharField(max_length=250, verbose_name='Location', blank=True)
    career = models.CharField(max_length=250, verbose_name='Career', blank=True)
    sex = models.CharField(max_length=1, default=3, choices=SEX, verbose_name='Sex', blank=True)
    about = models.TextField(max_length=500, blank=True, verbose_name='About')
    birthday = models.DateField(blank=True, verbose_name='Birthday', null=True)



def create_user_profile(sender, **kwargs):
    user = kwargs["instance"]
    if kwargs["created"]:
        user_profile = Profile(user=user)
        user_profile.save()


post_save.connect(create_user_profile, sender=User)



