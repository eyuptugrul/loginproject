from django import forms
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from .models import Profile


class LoginForm(forms.Form):
    username = forms.CharField(max_length=100, label='Kullanıcı Adı')
    password = forms.CharField(max_length=100, label='Parola', widget=forms.PasswordInput)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError("Kullanıcı adını veya şifreyi yanlış girdiniz!")
        return super(LoginForm, self).clean()


class RegisterForm(forms.ModelForm):
    username = forms.CharField(max_length=100)
    email = forms.EmailField()
    password1 = forms.CharField(max_length=100, widget=forms.PasswordInput)
    password2 = forms.CharField(max_length=100, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password1',
            'password2',
        ]

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Parolalar eşleşmiyor')
        return password2


class ProfileForm(forms.ModelForm):
    MAN = 'M'
    WOMEN = 'W'
    OTHER = 'O'
    SEX = (
        (MAN, 'MAN'),
        (WOMEN, 'WOMEN'),
        (OTHER, 'OTHER'),
    )
    cell_phone = forms.CharField(max_length=11,label='Phone no',required = False)
    sex = forms.CharField(widget=forms.Select(choices=SEX),required = False)
    about = forms.CharField(widget=forms.Textarea, max_length=500, label='About',required = False)
    birthday = forms.DateField(
        
        widget=forms.widgets.DateInput(attrs={
            'class': 'datepicker',
            'type': 'date'
            }),
         label='Birthday',
         required = False)

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'birthday',
            'sex',
            'cell_phone',
            'about'
        ]
        help_texts = {
            'username': None,
            
        }
