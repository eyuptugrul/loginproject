from django.urls import re_path, path, include
from .views import *

app_name = 'accounts'

urlpatterns = [
    re_path(r'^login/$', login_view, name='login'),
    re_path(r'^profile/$', profile_view, name='profile'),
    re_path(r'^register/$', register_view, name='register'),
    re_path(r'^logout/$', logout_view, name='logout'),
    re_path(r'^changepassword/$', change_password_view, name='change_password'),
    
]
