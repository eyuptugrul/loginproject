from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout,update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, RegisterForm, ProfileForm
from .models import Profile
from .forms import ProfileForm


def login_view(request):
    if request.user.is_authenticated:
        return redirect('home')
    form = LoginForm(request.POST or None)
    content = {
        'form': form,
        'title': 'Giriş yap'
    }
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        return redirect('/accounts/profile')

    return render(request, 'accounts/form.html', content)

def register_view(request):
    if request.user.is_authenticated:
        return redirect('home')
    form = RegisterForm(request.POST or None)
    content = {
        'form': form,
        'title': 'Üye ol'
    }
    if form.is_valid():
        user = form.save(commit=False)
        password = form.cleaned_data.get('password1')
        user.set_password(password)
        user.is_staff = True
        user.save()
        new_user = authenticate(username=user.username, password=password)
        login(request, new_user)
        return redirect('home')
    return render(request, 'accounts/form.html', content)


def logout_view(request):
    logout(request)
    return redirect('home')


def profile_view(request):
    data = {
        'sex': request.user.profile.sex,
        'about': request.user.profile.about,
        'cell_phone': request.user.profile.cell_phone,
        'birthday': request.user.profile.birthday
            }

    user_profile_form = ProfileForm(request.POST or None, instance=request.user, initial=data)
    content = {
        'form': user_profile_form,
        'title': 'Güncelle'
    }

    if request.method == 'POST':
        if user_profile_form.is_valid():
            user_profile_form.save(commit=True)
            sex = user_profile_form.cleaned_data['sex']
            about = user_profile_form.cleaned_data['about']
            cell_phone = user_profile_form.cleaned_data['cell_phone']
            birthday = user_profile_form.cleaned_data['birthday']

            request.user.profile.sex = sex
            request.user.profile.birthday = birthday
            request.user.profile.cell_phone = cell_phone
            request.user.profile.about = about
            request.user.profile.save()

    return render(request, 'accounts/form.html', content)

def change_password_view(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  
            return redirect('home')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'accounts/change_password_form.html', {
        'form': form
    })


