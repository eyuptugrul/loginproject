
from django.contrib import admin
from django.urls import re_path, include
from django.conf.urls.static import static
from django.conf import settings
from home.views import *

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^$', home_view, name='home'),
    re_path(r'^accounts/', include('accounts.urls')),
    re_path(r'^accounts/', include('django.contrib.auth.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
