from django.shortcuts import render, HttpResponse, HttpResponseRedirect, get_object_or_404, redirect, Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime
from django.contrib.auth.models import User
# Create your views here.


def home_view(request):
    if request.user.is_authenticated:


        context = {
            'isim':  request.user,
        }
    else:
        return redirect('accounts:login')

    return render(request, 'home.html', context)


